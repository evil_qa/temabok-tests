const puppeteer = require('puppeteer');
const PAGE = 'http://46.101.228.206/login';

/**
 * describe.skip - to skip the suite?
 * it.skip - to skip the test
 */

describe('LOGIN - GUEST VIEW', () => {

    let page, browser;

    beforeAll(async () => {
        browser = await puppeteer.launch({headless: false, slowMo: 200, devtools: false,
            args: [
                '--window-size=1920,1080'
            ],
            defaultViewport: null  /** or { width: 0, height: 0 } */
        });
        page = await browser.newPage(PAGE);
        await page.goto(PAGE, {waitUntil: 'domcontentloaded'});
        console.log('PAGE IS LOADED')
    }, 10000);

    afterAll(() => {
        browser.close();
    });

    it.skip('SHOULD VERIFY LOGIN OPTIONS IS ACCESSIBLE FOR GUEST USER', async () => {

        const loginButtonSelector = '#root > div > div.LoginPage.flexBox.spaceBetween.alignCenter > div.flexBox.dirColumn.justifyCenter.alignCenter.w50 > button';

        let loginButton = await page.waitForSelector(loginButtonSelector, {visible: true},
            console.log("LOGIN BUTTON IS VISIBLE"));
            await loginButton.click();

        await page.waitFor(2000,
            console.log('REDIRECT TO FEIDE'));

        await expect(page.title()).resolves.toMatch("Log in with Feide",
            console.log("FEIDE LOGIN IS ACCESSIBLE"));

        await page.screenshot({path: './test_output/scr/temabok-guest-test.png'},
            console.log('SCREENSHOT IS TAKEN'));

        console.log('COMPLETED');

    }, 10000);
});


