const puppeteer = require('puppeteer');
const PAGE = 'http://46.101.228.206/login';
const USER_NAME_TEACHER = 'laerer_no856326499_1a_1';
const USER_PASS_TEACHER = '2wsx';
const USER_NAME_STUDENT = 'elev_no856326499_8a_1';
const USER_PASS_STUDENT = '1qaz';


describe('LOGIN - FEIDE TEST USERS', () => {

    let page, browser;

    beforeEach(async () => {
        browser = await puppeteer.launch({headless: false, slowMo: 200, devtools: false,
            args: [
                '--window-size=1920,1080'
            ],
            defaultViewport: null
        });
        page = await browser.newPage(PAGE);
        await page.goto(PAGE, {waitUntil: 'domcontentloaded'});
        console.log('PAGE IS LOADED')
    }, 10000);

    afterEach(() => {
        browser.close();
    });

    it('SHOULD VERIFY LOGIN VIA FEIDE AS TEACHER', async () => {

        const userNameFieldSElector = '#username';
        const userPassFieldSelector = '#password';
        const loginButtonSelector = '#root > div > div.LoginPage.flexBox.spaceBetween.alignCenter > div.flexBox.dirColumn.justifyCenter.alignCenter.w50 > button';
        const testUsersSelector = '#discoform_other > ul > li:nth-child(7) > button > div > div.media-body';
        const submitButtonSelector = '#main > div.main > form > button';

        let loginButton = await page.waitForSelector(loginButtonSelector, {visible: true});
            await loginButton.click();

        await page.waitForNavigation({ waitUntil: 'domcontentloaded' },
            console.log('FEIDE IS OPEN'));

        let testUsers = await page.waitForSelector(testUsersSelector, {visible: true});
            await testUsers.click();

        await page.waitForNavigation({ waitUntil: 'domcontentloaded' },
            console.log('TEST USER LOGIN FORM IS OPEN'));

        await page.type(userNameFieldSElector, USER_NAME_TEACHER, {delay: 10});
        await page.type(userPassFieldSelector, USER_PASS_TEACHER, {delay: 10});

        let submitButton = await page.waitForSelector(submitButtonSelector, {visible: true});
            await submitButton.click();

        await page.waitForNavigation({ waitUntil: 'domcontentloaded' },
            console.log('USER IS LOGGED IN'));

        await page.waitFor(2000); /** wait for double redirect, disable after fix */

        await page.screenshot({path: './test_output/scr/temabok-loginFeide-teacher-test.png'},
            console.log('SCREENSHOT IS TAKEN'));

        await expect(page.title()).resolves.toMatch("Skolerommet");

        const pageURL = await page.url();
            console.log(pageURL, ' => CURRENT PAGE URL');

        console.log('COMPLETED');

    }, 180000);


    it('SHOULD VERIFY LOGIN VIA FEIDE AS STUDENT', async () => {

        const userNameFieldSElector = '#username';
        const userPassFieldSelector = '#password';
        const loginButtonSelector = '#root > div > div.LoginPage.flexBox.spaceBetween.alignCenter > div.flexBox.dirColumn.justifyCenter.alignCenter.w50 > button';
        const testUsersSelector = '#discoform_other > ul > li:nth-child(7) > button > div > div.media-body';
        const submitButtonSelector = '#main > div.main > form > button';

        let loginButton = await page.waitForSelector(loginButtonSelector, {visible: true});
        await loginButton.click();

        await page.waitForNavigation({ waitUntil: 'domcontentloaded' },
            console.log('FEIDE IS OPEN'));

        let testUsers = await page.waitForSelector(testUsersSelector, {visible: true});
        await testUsers.click();

        await page.waitForNavigation({ waitUntil: 'domcontentloaded' },
            console.log('TEST USER LOGIN FORM IS OPEN'));

        await page.type(userNameFieldSElector, USER_NAME_STUDENT, {delay: 10});
        await page.type(userPassFieldSelector, USER_PASS_STUDENT, {delay: 10});

        let submitButton = await page.waitForSelector(submitButtonSelector, {visible: true});
        await submitButton.click();

        await page.waitForNavigation({ waitUntil: 'domcontentloaded' },
            console.log('USER IS LOGGED IN'));

        await page.waitFor(2000); /** wait for double redirect, disable after fix */

        await page.screenshot({path: './test_output/scr/temabok-loginFeide-student-test.png'},
            console.log('SCREENSHOT IS TAKEN'));

        await expect(page.title()).resolves.toMatch("Skolerommet");

        const pageURL = await page.url();
        console.log(pageURL, ' => CURRENT PAGE URL');

        console.log('COMPLETED');

    }, 180000);

});